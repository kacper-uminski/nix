{
  lib,
  pkgs,
  fetchurl,
  cmake,
  libxml2,
  ...
}:
pkgs.gcc13Stdenv.mkDerivation rec {
  pname = "sacd_extract";
  version = "0.3.9.3";

  src = fetchurl {
    url = "https://github.com/sacd-ripper/sacd-ripper/archive/${version}.tar.gz";
    sha256 = "0bg7x7xn21qlzza2cgc1ybhjpgjqy7k6hrjhaq8x878m3q27hc1g";
  };

  nativeBuildInputs = [cmake];
  buildInputs = [libxml2];

  sourceRoot = "./sacd-ripper-${version}/tools/${pname}/";

  buildPhase = ''
    cmake .
    make
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp sacd_extract $out/bin
  '';

  meta = with lib; {
    description = "Extracts the tracks from SACD .iso files.";
    homepage = "https://github.com/sacd-ripper/sacd-ripper";
    license = licenses.gpl2Plus;
    mainProgram = "sacd_extract";
    platforms = platforms.linux;
  };
}
