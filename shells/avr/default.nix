{pkgs, ...}:
pkgs.mkShell {
  buildInputs = with pkgs; [
    avrdude
    ravedude
    pkgsCross.avr.buildPackages.gcc
  ];
}
