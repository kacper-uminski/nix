{
  default-importer,
  pkgs-stable,
  ...
}: {
  imports = default-importer ../../../modules/home;

  periodic = {
    cursor.size = 48;
    niri.enable = true;
    wallpaper = ../../../wallpapers/ali-abdaal/solstice.jpg;
  };

  programs = {
    alacritty.settings.window.opacity = 0.8;

    niri.settings = {
      binds = {
        "XF86MonBrightnessUp".action.spawn = [
          "${pkgs-stable.light}/bin/light"
          "-A"
          "10"
        ];
        "XF86MonBrightnessDown".action.spawn = [
          "${pkgs-stable.light}/bin/light"
          "-U"
          "10"
        ];

        "XF86AudioRaiseVolume".action.spawn = [
          "${pkgs-stable.wireplumber}/bin/wpctl"
          "set-volume"
          "-l"
          "1.0"
          "@DEFAULT_AUDIO_SINK@"
          "5%+"
        ];
        "XF86AudioLowerVolume".action.spawn = [
          "${pkgs-stable.wireplumber}/bin/wpctl"
          "set-volume"
          "-l"
          "1.0"
          "@DEFAULT_AUDIO_SINK@"
          "5%-"
        ];
        "XF86AudioMute".action.spawn = [
          "${pkgs-stable.wireplumber}/bin/wpctl"
          "set-mute"
          "@DEFAULT_AUDIO_SINK@"
          "toggle"
        ];
      };

      spawn-at-startup = [
        {
          command = [
            "${pkgs-stable.light}/bin/light"
            "-N"
            "1"
          ];
        }
      ];
    };
  };

  wayland.windowManager.hyprland.settings = {
    bind = [
      "$mod ALT, D, exec, hyprctl switchxkblayout microsoft-surface-type-cover-keyboard 0"
      "$mod ALT, S, exec, hyprctl switchxkblayout microsoft-surface-type-cover-keyboard 1"
      "$mod ALT, P, exec, hyprctl switchxkblayout microsoft-surface-type-cover-keyboard 2"
    ];

    binde = [
      ", XF86MonBrightnessUp, exec, ${pkgs-stable.light}/bin/light -A 10"
      ", XF86MonBrightnessDown, exec, ${pkgs-stable.light}/bin/light -U 10"
      ", XF86AudioRaiseVolume, exec, ${pkgs-stable.wireplumber}/bin/wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 5%+"
      ", XF86AudioLowerVolume, exec, ${pkgs-stable.wireplumber}/bin/wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 5%-"
      ", XF86AudioMute, exec, ${pkgs-stable.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
    ];

    exec-once = [
      "${pkgs-stable.light}/bin/light -N 1"
    ];

    gestures = {
      workspace_swipe = true;
      workspace_swipe_invert = false;
    };

    input.touchpad.middle_button_emulation = true;
  };

  home.stateVersion = "24.05";
}
