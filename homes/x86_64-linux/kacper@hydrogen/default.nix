{
  default-importer,
  pkgs-stable,
  ...
}: {
  imports = default-importer ../../../modules/home;

  periodic = {
    niri.enable = true;
    wallpaper = ../../../wallpapers/misc/black.jpg;
  };

  programs = {
    niri.settings.spawn-at-startup = with pkgs-stable; [
      {command = ["${easyeffects}/bin/easyeffects"];}
      {command = ["${tdesktop}/bin/telegram-desktop"];}
    ];
    alacritty.settings.window.padding = {
      x = 20;
      y = 20;
    };
  };

  wayland.windowManager.hyprland.settings = {
    bind = [
      "$mod ALT, D, exec, hyprctl switchxkblayout $kbd 0"
      "$mod ALT, S, exec, hyprctl switchxkblayout $kbd 1"
      "$mod ALT, P, exec, hyprctl switchxkblayout $kbd 2"
    ];

    workspace = [
      "3, on-created-empty:telegram-desktop"
    ];
  };

  home.stateVersion = "24.11";
}
