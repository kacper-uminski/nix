{...}: {
  programs.ncmpcpp = {
    enable = true;
    settings = {
      mpd_host = "jukebox.lysator.liu.se";
      mpd_music_dir = "/lysator/jukebox/";
    };
  };
}
