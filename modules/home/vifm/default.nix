{...}: {
  home.file = {
    ".config/vifm" = {
      source = ./src;
      recursive = true;
    };
  };
}
