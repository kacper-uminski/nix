{
  inputs,
  lib,
  pkgs-stable,
  ...
}: {
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
  ];
  programs.nixvim = {
    enable = true;

    colorschemes.tokyonight.enable = true;
    defaultEditor = true;

    clipboard = {
      providers.wl-copy.enable = true;
      register = "unnamedplus";
    };

    globals = {
      mapleader = " ";
      maplocalleader = " ";
    };

    keymaps = [
      {
        action = "å";
        key = "<A-'>";
        mode = "i";
        options.silent = true;
      }
      {
        action = "ä";
        key = "<A-,>";
        mode = "i";
        options.silent = true;
      }
      {
        action = "ö";
        key = "<A-.>";
        mode = "i";
        options.silent = true;
      }
      {
        action = "Å";
        key = "<A-\">";
        mode = "i";
        options.silent = true;
      }
      {
        action = "Ä";
        key = "<A-\<>";
        mode = "i";
        options.silent = true;
      }
      {
        action = "Ö";
        key = "<A-\>>";
        mode = "i";
        options.silent = true;
      }
    ];

    opts = {
      number = true;
      signcolumn = "yes";
      tabstop = 4;
      scrolloff = 999;
      shiftwidth = 2;
      expandtab = true;
      cindent = true;
      updatetime = 300;
      termguicolors = true;
      mouse = "a";
    };
    plugins = {
      cmp = {
        enable = true;
        settings = {
          mapping = {
            "<C-n>" =
              # lua
              "cmp.mapping.select_next_item()";
            "<C-p>" =
              # lua
              "cmp.mapping.select_prev_item()";
            "<C-d>" =
              # lua
              "cmp.mapping.scroll_docs(-4)";
            "<C-f>" =
              # lua
              "cmp.mapping.scroll_docs(4)";
            "<C-space>" =
              # lua
              "cmp.mapping.complete()";
            "<CR>" =
              # lua
              "cmp.mapping.confirm { behavior = cmp.ConfirmBehavior.Replace, select = true }";
            "<Tab>" =
              # lua
              ''
                cmp.mapping(function(fallback)
                  local luasnip = require("luasnip")
                  if cmp.visible() then
                    cmp.select_next_item()
                  elseif luasnip.expand_or_locally_jumpable() then
                    luasnip.expand_or_jump()
                  else
                    fallback()
                  end
                end, {'i', 's'})
              '';
            "<S-Tab>" =
              # lua
              ''
                cmp.mapping(function(fallback)
                  local luasnip = require("luasnip")
                  if cmp.visible() then
                    cmp.select_prev_item()
                  elseif luasnip.locally_jumpable(-1) then
                    luasnip.jump(-1)
                  else
                    fallback()
                  end
                end, {'i', 's'})
              '';
          };
          snippet.expand =
            # lua
            ''
              function(args)
                require('luasnip').lsp_expand(args.body)
              end
            '';
          sources = [
            {name = "nvim_lsp";}
            {name = "luasnip";}
            {name = "buffer";}
          ];
        };
      };
      diffview.enable = true;
      friendly-snippets.enable = true;
      fzf-lua = {
        enable = true;
        keymaps = {
          "<leader>b".action = "buffers";
          "<leader>f".action = "files";
          "gr".action = "lsp_references";
        };
      };
      gitsigns.enable = true;
      lsp = {
        enable = true;
        keymaps = {
          diagnostic = {
            "<leader>j" = "goto_next";
            "<leader>k" = "goto_prev";
          };
          lspBuf = {
            K = "hover";
            gd = "definition";
            gD = "declaration";
            gI = "implementation";
            "<leader>a" = "code_action";
            "<leader>D" = "type_definition";
            "<leader>r" = "rename";
          };
        };
        onAttach = lib.strings.concatLines [
          # lua
          ''
            vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_) vim.lsp.buf.format() end, {})
          ''
        ];
        servers = {
          clangd.enable = false;
          hls = {
            enable = true;
            installGhc = false;
          };
          nixd = {
            enable = true;
            settings = {
              formatting.command = ["alejandra"];
              nixpkgs.expr =
                # nix
                ''import (builtins.getFlake ../../../).inputs.nixpkgs {}'';
            };
          };
          rust_analyzer = {
            enable = true;
            installCargo = true;
            installRustc = true;
            package = pkgs-stable.rustup;
            cargoPackage = pkgs-stable.rustup;
            rustcPackage = pkgs-stable.rustup;
          };
          tinymist = {
            enable = true;
            settings = {
              exportPdf = "onType";
              formatterMode = "typstyle";
            };
          };
          vhdl_ls.enable = false;
        };
      };
      lualine.enable = true;
      luasnip.enable = true;
      nvim-autopairs.enable = true;
      rainbow-delimiters.enable = true;
      transparent.enable = true;
      treesitter = {
        enable = true;
        settings = {
          highlight.enable = true;
          indent.enable = true;
        };
      };
      web-devicons.enable = true;
      yanky.enable = true;
    };
    viAlias = true;
    vimAlias = true;
  };
}
