{config, ...}: {
  xdg.enable = true;
  home.sessionVariables = {
    CARGO_HOME = "${config.xdg.dataHome}/cargo";
    NIXOS_OZONE_WL = "1";
    RUSTUP_HOME = "${config.xdg.dataHome}/rustup";
    XCOMPOSECACHE = "${config.xdg.cacheHome}/X11/xcompose";
  };
}
