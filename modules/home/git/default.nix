{...}: {
  programs = {
    git = {
      enable = true;
      aliases = {
        c = "commit";
        co = "checkout";
        d = "diff";
        f = "fetch";
        p = "pull";
        rb = "rebase";
        s = "status";
        sw = "switch";
        st = "stash";
      };
      delta = {
        enable = true;
        options = {
          line-numbers = true;
          merge.conflictStyle = "zdiff3";
          side-by-side = true;
        };
      };
      extraConfig = {
        branch.sort = "-committerdate";
        column.ui = "auto";
        commit.verbose = true;
        fetch = {
          all = true;
          prune = true;
          pruneTags = true;
        };
        help.autocorrect = "prompt";
        init.defaultBranch = "main";
        push = {
          autoSetupRemote = true;
          default = "simple";
          followTags = true;
        };
        rerere = {
          enabled = true;
          autoupdate = true;
        };
        tag.sort = "version:refname";
      };
      userName = "kacper-uminski";
      userEmail = "kacperuminski@protonmail.com";
    };
    gitui.enable = true;
    lazygit.enable = true;
  };
}
