{pkgs-stable, ...}: {
  programs.helix = {
    enable = true;
    languages = {
      language-server = {
        tinymist = {
          command = "${pkgs-stable.tinymist}/bin/tinymist";
          config = {
            exportPdf = "onType";
            formatterMode = "typstyle";
          };
        };
        nixd = {
          command = "${pkgs-stable.nixd}/bin/nixd";
        };
      };
      language = [
        {
          name = "nix";
          formatter = {command = "${pkgs-stable.alejandra}/bin/alejandra";};
          language-servers = ["nixd"];
        }
        {
          name = "typst";
          formatter = {command = "${pkgs-stable.typstyle}/bin/typstyle";};
          language-servers = ["tinymist"];
        }
      ];
    };
    settings = {
      editor = {
        color-modes = true;
        cursor-shape.insert = "bar";
        lsp.display-messages = true;
        scrolloff = 999;
      };
      keys = {
        insert = {
          "A-'" = [":append-output echo -n 'å'"];
          "A-," = [":append-output echo -n 'ä'"];
          "A-." = [":append-output echo -n 'ö'"];
          "A-\"" = [":append-output echo -n 'Å'"];
          "A-<" = [":append-output echo -n 'Ä'"];
          "A->" = [":append-output echo -n 'Ö'"];
          C-d = ["normal_mode"];
        };
        normal = {
          C-g = [
            ":new"
            ":insert-output ${pkgs-stable.gitui}/bin/gitui"
            ":buffer-close!"
            ":redraw"
          ];
          C-r = [":reflow 80"];
        };
        select = {
          C-r = [
            ":reflow 80"
            "normal_mode"
          ];
        };
      };
      theme = "tokyonight";
    };
    themes = {
      tokyonight_transparent = {
        inherits = "tokyonight";
        ui.background = {};
      };
    };
  };
}
