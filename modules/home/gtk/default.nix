{pkgs, ...}: {
  gtk = {
    enable = true;
    cursorTheme.name = "Breeze";
    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus";
    };
    theme = {
      package = pkgs.adw-gtk3;
      name = "adw-gtk3-dark";
    };
  };

  home.file.".icons/Breeze".source = ./Breeze;
}
