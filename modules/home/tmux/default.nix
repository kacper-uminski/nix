{...}: {
  programs.tmux = {
    enable = true;
    baseIndex = 1;
    clock24 = true;
    sensibleOnTop = true;
  };
}
