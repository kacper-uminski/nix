{lib, ...}: {
  programs.starship = {
    enable = true;
    settings = {
      add_newline = false;
      format = lib.concatStrings [
        "$all"
        "$directory"
        "$character"
      ];
    };
  };
}
