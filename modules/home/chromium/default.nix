{pkgs-stable, ...}: {
  programs.chromium = {
    enable = false;
    commandLineArgs = [
      "--ozone-platform-hint=auto"
      "--enable-features=TouchpadOverscrollHistoryNavigation"
    ];
    package = pkgs-stable.chromium;
  };
}
