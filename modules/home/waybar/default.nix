{
  lib,
  config,
  ...
}: {
  config = lib.mkIf config.programs.waybar.enable {
    wayland.windowManager.hyprland.settings.exec-once = [
      "waybar &"
    ];
    programs.waybar = {
      settings = {
        mainBar = {
          height = 32;
          layer = "top";
          modules-left = ["hyprland/workspaces"];
          modules-right = [
            "tray"
            "pulseaudio"
            "network"
            "battery"
            "clock"
          ];
          output = ["eDP-1"];
          position = "top";
          spacing = 4;

          battery = {
            adapter = "ADP1";
            bat = "BAT1";
            format = "{icon}  {capacity}%";
            format-icons = [
              " "
              " "
              " "
              " "
              " "
            ];
            format-charging = " {capacity}%";
            states = {
              critical = 15;
              warning = 30;
            };
          };

          clock = {
            format = "{:%H:%M}";
            interval = 1;
            tooltip-format = "<big>{:%Y %B}</big>";
          };

          "hyprland/workspaces" = {
            all-outputs = true;
            default = "";
            focused = "";
            format = "{icon}";
            format-icons = {
              # Needs reformat if ever used again, removing base16.
              "1" = "<span color=\"#base07\">1</span>";
              "2" = "<span color=\"#base07\">2</span>";
              "3" = "<span color=\"#base07\">3</span>";
              "4" = "<span color=\"#base07\">4</span>";
              "5" = "<span color=\"#base07\">5</span>";
              "6" = "<span color=\"#base07\">6</span>";
              "7" = "<span color=\"#base07\">7</span>";
              "8" = "<span color=\"#base07\">8</span>";
              "9" = "<span color=\"#base07\">9</span>";
            };
            on-click = "activate";
            on-scroll-up = "hyprctl dispatch workspace e+1";
            on-scroll-down = "hyprctl dispatch workspace e-1";
            sort-by-number = true;
            urgent = " ";
          };

          network = {
            format-disconnected = " ";
            format-wifi = " ";
            interface = "wlp0s20f3";
          };

          pulseaudio = {
            format = "{icon}  {volume}%";
            format-muted = " ";
            scroll-step = 5;
            on-click = "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
            format-icons = {
              headphone = " ";
              hands-free = " ";
              headset = " ";
              phone = " ";
              portable = " ";
              car = " ";
              default = [
                " "
                " "
                " "
              ];
            };
          };

          tray = {
            icon-size = 14;
            spacing = 10;
          };
        };
      };
      # Needs reformat if ever used again, removed base16.
      style =
        # css
        ''
          * {
            border: none;
            border-radius: 0px;
            font-family: Plex Mono;
            font-weight: bold;
            font-size: 15px;
            min-height: 0;
          }

          window#waybar {
            background: #base00;
            color:  #base07
          }

          tooltip {
            background: #base00;
            border-radius: 10px;
            border-width: 2px;
            border-style: solid;
            border-color: #base00;
          }
          #workspaces button {
            padding: 0 0.6em;
            color: #base02;
            border-radius: 6px;
            margin-right: 2px;
            margin-left: 2px;
            margin-top: 2px;
            margin-bottom: 2px;
          }

          #workspaces button.active {
            color: #base00;
            background: #base02;
          }

          #workspaces button.focused {
            color: #base00;
            background: #base02;
          }

          #workspaces button.urgent {
            color: #base07;
            background: #base0F;
          }

          #workspaces button:hover {
            color: #base00;
            background: #base02;
          }

          #battery,
          #clock,
          #network,
          #pulseaudio,
          #workspaces
          #tray {
            color: #base07;
            background: #base02;
            padding: 0 0.6em;
            margin-left: 4px;
            margin-right: 4px;
            margin-top: 4px;
            margin-bottom: 4px;
            border-radius: 6px;
          }

          #taskbar {
            background: #base02;
            padding: 0 0.6em;
            margin-right: 10px;
            margin-left: 4px;
            margin-top: 4px;
            margin-bottom: 4px;
            border-radius: 6px;
            color: #base07;
          }

          #taskbar button {
            border-radius: 6px;
            background: #base02;
            color: #base07;
            margin-right: 6px;
          }

          #taskbar button.minimized {
            border-radius: 6px;
            background: #base02;
            color: #base07;
          }

          #taskbar button.active {
            border-radius: 6px;
            background: #base0B;
            color: #base07;
          }
        '';
    };
  };
}
