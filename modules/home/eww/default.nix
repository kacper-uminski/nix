{pkgs, ...}: {
  home.packages = with pkgs; [
    (writeShellScriptBin "battery" ''
      #!/bin/sh

      bat=/sys/class/power_supply/BAT1
      per="$(cat "$bat/capacity")"

      [ $(cat "$bat/status") = Charging ] && echo "" && exit

      icon = ""
      if [ "$per" -gt "90" ]; then
      	icon=""
      elif [ "$per" -gt "75" ]; then
      	icon=""
      elif [ "$per" -gt "50" ]; then
      	icon=""
      elif [ "$per" -gt "25" ]; then
      	icon=""
      else
      	notify-send -u critical "Battery Low" "Connect Charger"
      	icon=""
      fi
      echo "$icon"
    '')
  ];
}
