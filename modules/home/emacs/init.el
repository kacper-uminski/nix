;; -*- lexical-binding: t; -*-
;;; Code:

(use-package
 emacs
 :init
 ;; Move customization variables to a separate file and load it.
 (setq custom-file (locate-user-emacs-file "custom-vars.el"))
 (load custom-file 'noerror 'nomessage)

 :custom
 ;; Set backup directory.
 (backup-directory-alist
  '(("." . ,(concat user-emacs-directory "backups"))))

 ;; Revert buffers when the underlying file has changed.
 (global-auto-revert-mode t)
 ;; Revert dired and other non-file buffers.
 (global-auto-revert-non-file-buffers t)

 ;; Disable bars and startup screen.
 (inhibit-startup-screen t)
 (menu-bar-mode nil)
 (scroll-bar-mode nil)
 (tool-bar-mode nil)

 ;; Stop cursor blink.
 (blink-cursor-mode nil)

 ;; Auto-complete pairs.
 (electric-pair-mode t)

 ;; Indent using spaces
 (indent-tabs-mode nil)
 ;; Save history between startups
 (savehist-mode t)

 ;; Don't pop up UI dialogs when prompting.
 (use-dialog-box nil)

 :config
 ;; Set font.
 (set-face-attribute 'default nil :font "IBM Plex Mono" :height 120)
 ;; Replace yes-no prompts with y-n.
 (defalias 'yes-or-no-p 'y-or-n-p)

 ;; Enable transparency.
 (set-frame-parameter (selected-frame) 'alpha-background 80)
 (add-to-list 'default-frame-alist '(alpha-background . 80))

 :hook
 ;; Enable line-numbers in program files.
 (prog-mode . display-line-numbers-mode))


;; All-the-icon-dired - Nicer icons in dired.
(use-package all-the-icons :if (display-graphic-p))

(use-package
 all-the-icons-completion
 :defer t
 :custom (all-the-icon-completion-mode t)
 :hook
 (marginalia-mode . all-the-icons-completion-marginalia-setup))

(use-package
 all-the-icons-dired
 :defer t
 :hook (dired-mode . #'all-the-icons-dired-mode))

;; Clang-format - formatter for C/C++
(use-package
 clang-format
 :defer t
 :custom (clang-format-style "file")
 :bind (:map c-mode-base-map ("C-c f b" . clang-format-buffer)))

(use-package
 clang-format+
 :defer t
 :hook (c-mode-common . clang-format+-mode))

;; Set cursor to stay in the middle of the screen.
(use-package
 centered-cursor-mode
 :defer t
 :hook
 ((org-mode markdown-mode prog-mode typst-ts-mode)
  .
  centered-cursor-mode))

;; IRC Client
(use-package
 erc
 :custom
 (erc-server "irc.libera.chat")
 (erc-nick "kacper")
 (erc-user-full-name "Kacper Uminski")
 (erc-track-shorten-start 8)
 (erc-autojoin-channels-alist '(("irc.libera.chat" "#lysator")))
 (erc-kill-buffer-on-part t)
 (erc-auto-query 'bury))

;; Dashboard
(use-package
 dashboard
 :custom
 (dashboard-icon-type 'all-the-icons)
 (dashboard-items '((recents . 5)))
 (dashboard-set-file-icons t)
 (dashboard-set-heading-icons t)
 (dashboard-startup-banner 'logo)
 :config (dashboard-setup-startup-hook))

;; Use the doom modeline.
(use-package
 doom-modeline
 :custom
 (column-number-mode t) ; Column numbers in modeline, not specific to doom.
 (doom-modeline-mode t)
 (doom-modeline-buffer-encoding nil)
 (doom-modeline-buffer-file-name-style 'truncate-with-project)
 (doom-modeline-buffer-modification-icon t)
 (doom-modeline-enable-word-count t)
 (doom-modeline-minor-modes nil)
 (doom-modeline-window-width-limit nil))

;; Set the theme.
(use-package
 doom-themes
 :config (doom-themes-org-config) (load-theme 'doom-tokyo-night t))

;; Editorconfig (set values for projects)
(use-package editorconfig :defer t :custom (editorconfig-mode t))

(use-package
 elfeed
 :defer t
 :custom
 (elfeed-feeds '("https://strongly-typed-thoughts.net/blog/feed")))

(use-package
 elisp-autofmt
 :defer t
 :commands (elisp-autofmt-mode elisp-autofmt-buffer)
 :hook (emacs-lisp-mode . elisp-autofmt-mode))

(use-package
 evil
 :custom
 (evil-undo-system 'undo-redo)
 (evil-want-C-i-jump nil)
 (evil-want-integration t) ;; This is optional since it's already set to t by default.
 (evil-want-keybinding nil)
 :bind
 (:map
  evil-insert-state-map
  ;; Return to normal mode
  ("C-g" . 'evil-normal-state)
  ;; Delete backwards
  ("C-h" . 'evil-delete-backward-char-and-join)
  ;; Output Swedish letters
  ("M-'" .
   (lambda ()
     (interactive)
     (insert "å")))
  ("M-," .
   (lambda ()
     (interactive)
     (insert "ä")))
  ("M-." .
   (lambda ()
     (interactive)
     (insert "ö")))
  ("M-\"" .
   (lambda ()
     (interactive)
     (insert "Å")))
  ("M-<" .
   (lambda ()
     (interactive)
     (insert "Ä")))
  ("M->" .
   (lambda ()
     (interactive)
     (insert "Ö")))

  :map evil-visual-state-map ("C-g" . 'evil-normal-state))
 :config (evil-mode 1)
 ;; set leader key in all states
 (evil-set-leader nil (kbd "C-SPC"))
 ;; set leader key in normal state
 (evil-set-leader 'normal (kbd "SPC"))
 ;; set local leader
 (evil-set-leader 'normal "," t))

(use-package
 evil-collection
 :after evil
 :config (evil-collection-init))

;; Flycheck (error highlighting)
(use-package flycheck :defer t :custom (global-flycheck-mode t))

;; Helpful (better help screens).
(use-package
 helpful
 :defer t
 :bind
 (("C-h f" . helpful-callable)
  ("C-h v" . helpful-variable)
  ("C-h k" . helpful-key)
  ("C-h F" . helpful-function)
  ("C-c C-d" . helpful-at-point)))

;; Enable LSP
(use-package
 lsp-bridge
 :defer t
 :custom
 (lsp-bridge-nix-lsp-server 'nixd)
 (lsp-bridge-c-lsp-server 'clangd)
 (lsp-bridge-python-multi-lsp-server 'pylsp_ruff)
 :config (global-lsp-bridge-mode))

;; Magit
(use-package magit :defer t)

(use-package
 magit-delta
 :defer t
 :hook (magit-mode . magit-delta-mode))

;; Marginalia
(use-package marginalia :defer t :custom (marginalia-mode t))

(use-package nix-ts-mode :defer t :mode "\\.nix\\'")

(use-package
 orderless
 :custom (completion-styles '(orderless basic))
 (completion-category-overrides
  '((file (styles basic partial-completion)))))

(use-package
 org
 :defer t
 :custom (org-ellipsis " ▾")
 :hook
 (org-mode
  .
  (lambda ()
    (org-indent-mode)
    (auto-fill-mode 0)
    (setq evil-auto-indent nil))))

;; Enable rainbow delimiters. ()[]{} etc.
(use-package
 rainbow-delimiters
 :defer t
 :hook (prog-mode . rainbow-delimiters-mode))

;; Enable color code highlighting.
(use-package rainbow-mode :defer t :hook (prog-mode . rainbow-mode))

;; Rustic mode
(use-package rustic :defer t :mode "\\.rs\\'")

(use-package typst-ts-mode :defer t :mode "\\.typ\\'")

;; Vertico completion framework.
(use-package vertico :custom (vertico-cycle t) (vertico-mode t))

(use-package
 vertico-posframe
 :after vertico
 :custom (vertico-posframe-mode t)
 (vertico-posframe-parameters
  '((left-fringe . 8) (right-fringe . 8))))

;; Enable key hints
(use-package
 which-key
 :custom (which-key-idle-delay 1) (which-key-mode t))

;; Enable snippets
(use-package yasnippet :defer t :custom (yas-global-mode t))
(use-package yasnippet-snippets)

(provide 'init)
;;; init.el ends here
