{
  fetchgit,
  trivialBuild,
}:
trivialBuild {
  pname = "typst-ts-mode";
  version = "main-2024-12-22";
  src = fetchgit {
    url = "https://codeberg.org/meow_king/typst-ts-mode";
    rev = "34d522c0a0";
    hash = "sha256-hx6soqaqyk678vn3LZgkagMwsYOZaMh9TMV3hsJWukI=";
  };
}
