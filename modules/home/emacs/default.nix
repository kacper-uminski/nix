{pkgs, ...}: {
  programs.emacs = {
    enable = true;
    package = pkgs.emacs30-pgtk;
    extraPackages = epkgs: let
      typst-ts-mode = pkgs.callPackage ./typst-ts-mode.nix {
        inherit (pkgs) fetchgit;
        inherit (epkgs) trivialBuild;
      };
    in (with epkgs; [
      all-the-icons
      all-the-icons-completion
      all-the-icons-dired
      pkgs.emacs-all-the-icons-fonts
      centered-cursor-mode
      clang-format
      epkgs."clang-format+"
      dashboard
      doom-modeline
      doom-themes
      editorconfig
      elfeed
      elisp-autofmt
      ess
      evil
      evil-collection
      flycheck
      haskell-mode
      helpful
      htmlize
      lsp-bridge
      magit
      magit-delta
      marginalia
      nix-ts-mode
      pkgs.nixd
      orderless
      rainbow-mode
      rainbow-delimiters
      rustic
      spacious-padding
      tree-sitter
      tree-sitter-langs
      treesit-grammars.with-all-grammars
      typst-ts-mode
      vertico
      vertico-posframe
      which-key
      yasnippet
      yasnippet-snippets
      yuck-mode
    ]);
  };

  services.emacs = {
    # Enable emacs daemon.
    enable = true;
    defaultEditor = false;
  };

  home.file.".config/emacs/init.el".source = ./init.el;

  home.shellAliases.emc = "emacsclient -nw";
}
