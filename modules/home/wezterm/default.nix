{...}: {
  programs.wezterm = {
    enable = true;
    extraConfig =
      # lua
      ''
        return {
          color_scheme = 'Tokyo Night',
          font = wezterm.font("IBM Plex Mono"),
          font_size = 13.0,
          hide_tab_bar_if_only_one_tab = true,
          keys = {
            { key = 'V', mods = 'CTRL|SHIFT', action = wezterm.action.PasteFrom 'Clipboard' },
            { key = 'V', mods = 'CTRL|SHIFT', action = wezterm.action.PasteFrom 'PrimarySelection' },
          },
          window_background_opacity = 0.8,
          window_decorations = 'NONE'
        }
      '';
  };
}
