{
  inputs,
  pkgs-stable,
  ...
}: let
  proxyConfig =
    pkgs-stable.writers.writeText "proxyConfig.js"
    # js
    ''
      function FindProxyForURL(url, host) {
        if (shExpMatch(host, "*gitlab*.liu.se")) {
          return "DIRECT";
        }
        if (shExpMatch(host, "*lysator.liu.se")) {
          return "DIRECT";
        }
        if (shExpMatch(host, "*mai.liu.se")) {
          return "DIRECT";
        }
        if (shExpMatch(host, "*.net.liu.se")) {
          return "DIRECT";
        }
        if (shExpMatch(host, "studieinfo.liu.se")) {
          return "DIRECT";
        }
        if (shExpMatch(host, "*.liu.se") || shExpMatch(host, "login.microsoftonline.com")) {
          return "SOCKS5 127.0.0.1:1776";
        }
        return "DIRECT";
      }
    '';
in {
  programs.firefox = {
    enable = true;
    profiles.kacper = {
      extensions.packages = with inputs.firefox-addons.packages.x86_64-linux; [
        bitwarden
        consent-o-matic
        darkreader
        return-youtube-dislikes
        sponsorblock
        # tokyo-night-v2
        ublock-origin
      ];

      search = {
        default = "DuckDuckGo";
        engines = {
          "Nix Packages" = {
            urls = [
              {
                template = "https://search.nixos.org/packages";
                params = [
                  {
                    name = "type";
                    value = "packages";
                  }
                  {
                    name = "query";
                    value = "{searchTerms}";
                  }
                ];
              }
            ];

            icon = "${pkgs-stable.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
            definedAliases = ["@np"];
          };
        };
        force = true;
      };

      settings = {
        "browser.ctrlTab.sortByRecentlyUsed" = true;
        "browser.display.use_document_fonts" = 0;
        "browser.newtabpage.enabled" = false;
        "browser.startup.homepage" = "about:blank";
        "browser.startup.page" = 3;
        "extensions.pocket.enable" = false;
        "font.name.monospace.x-western" = "IBM Plex Mono";
        "font.name.sans-serif.x-western" = "IBM Plex Sans";
        "font.name.serif.x-western" = "IBM Plex Serif";
        "network.proxy.autoconfig_url" = "file://${proxyConfig}";
        "network.proxy.socks" = "127.0.0.1";
        "network.proxy.socks_port" = 1776;
        "network.proxy.type" = 2;
        "signon.rememberSignons" = false;
        "startup.homepage_welcome_url" = "about:blank";
      };
    };
  };
}
