{pkgs-stable, ...}: {
  imports = [
    ./utils.nix
  ];

  home.shellAliases = {
    lsusb = "${pkgs-stable.cyme}/bin/cyme";
    week = "date +%V";
  };

  programs = {
    bash = {
      enable = true;
      enableCompletion = true;
      historyFile = ".config/bash/bash_history";
    };

    fish = {
      enable = true;
      functions = {
        fish_greeting = "";
      };
    };

    nushell = {
      enable = true;
      extraConfig =
        #nu
        ''
          $env.config = {
            show_banner: false,
          }
        '';
      shellAliases = {
        la = "ls -a";
        ll = "ls -l";
        lla = "ls -al";
        l = "ls -al";
        lt = "${pkgs-stable.eza}/bin/eza --tree";
      };
    };

    zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      dotDir = ".config/zsh";
      history.path = "$ZDOTDIR/zsh_history";
    };
  };
}
