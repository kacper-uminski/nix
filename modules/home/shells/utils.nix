{pkgs-stable, ...}: {
  programs = {
    carapace.enable = true;

    direnv.enable = true;

    eza = {
      enable = true;
      git = true;
      icons = "auto";
    };

    htop.enable = true;

    ripgrep.enable = true;

    yazi = {
      enable = true;
      package = pkgs-stable.yazi;
    };

    zoxide = {
      enable = true;
      options = [
        "--cmd cd"
      ];
    };
  };
}
