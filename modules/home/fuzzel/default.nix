{pkgs, ...}: {
  programs.fuzzel = {
    enable = true;
    settings = {
      colors = {
        background = "#1a1b26ff";
        border = "#a9b1d6ff";
        selection = "#30313bff";
      };
      main = {
        terminal = "${pkgs.wezterm}/bin/wezterm start --always-new-process";
        font = "IBM Plex Mono:size=13";
      };
    };
  };
}
