{
  config,
  lib,
  pkgs,
  pkgs-stable,
  ...
}: let
  actions = config.lib.niri.actions;
in {
  options.periodic = {
    niri.enable = lib.mkEnableOption "Enable hyprland.";
  };
  config = {
    periodic.niri.enable = lib.mkDefault false;
    programs = {
      fish.loginShellInit =
        lib.mkIf config.periodic.niri.enable # fish
        
        ''
          if test -z $WAYLAND_DISPLAY; and test (tty) = /dev/tty1
              niri
          end
        '';
      zsh.loginExtra =
        lib.mkIf config.periodic.niri.enable # sh
        
        ''
          if [[ -z $WAYLAND_DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then niri; fi
        '';
      niri.settings = {
        binds = {
          "Mod+1".action.focus-workspace = "cmd";
          "Mod+2".action.focus-workspace = "web";
          "Mod+3".action.focus-workspace = "chat";
          "Mod+4".action.focus-workspace = "game";
          "Mod+5".action.focus-workspace = "av";
          "Mod+6".action.focus-workspace = "dl";
          "Mod+7".action.focus-workspace = "rand";
          "Mod+8".action.focus-workspace = 8;
          "Mod+9".action.focus-workspace = 9;
          "Mod+0".action.focus-workspace = 10;
          "Mod+B".action.spawn = ["${pkgs.firefox}/bin/firefox"];
          "Mod+F".action = actions.fullscreen-window;
          "Mod+H".action = actions.focus-column-left-or-last;
          "Mod+L".action = actions.focus-column-right-or-first;
          "Mod+J".action = actions.focus-workspace-down;
          "Mod+K".action = actions.focus-workspace-up;
          "Mod+R".action.spawn = ["${pkgs-stable.fuzzel}/bin/fuzzel"];
          "Mod+S".action = actions.screenshot;
          "Mod+T".action.spawn = [
            "${pkgs-stable.wezterm}/bin/wezterm"
            "start"
            "--always-new-process"
          ];
          "Mod+W".action = actions.close-window;
          "Mod+Shift+1".action.move-window-to-workspace = "cmd";
          "Mod+Shift+2".action.move-window-to-workspace = "web";
          "Mod+Shift+3".action.move-window-to-workspace = "chat";
          "Mod+Shift+4".action.move-window-to-workspace = "game";
          "Mod+Shift+5".action.move-window-to-workspace = "av";
          "Mod+Shift+6".action.move-window-to-workspace = "dl";
          "Mod+Shift+7".action.move-window-to-workspace = "rand";
          "Mod+Shift+8".action.move-window-to-workspace = 8;
          "Mod+Shift+9".action.move-window-to-workspace = 9;
          "Mod+Shift+0".action.move-window-to-workspace = 10;
          "Mod+Shift+Q".action.quit.skip-confirmation = true;
          "Mod+Space".action.switch-layout = "next";
        };

        cursor = config.periodic.cursor;

        environment = {
          DISPLAY = ":0";
        };

        hotkey-overlay.skip-at-startup = true;

        input = {
          keyboard = {
            track-layout = "window";
            xkb = {
              layout = "pl, se";
              variant = "dvorak, dvorak";
              options = "caps:ctrl_modifier";
            };
          };

          touchpad = {
            natural-scroll = false;
          };
        };

        outputs.HDMI-A-3.mode = {
          height = 2160;
          width = 3840;
          refresh = 120.0;
        };

        prefer-no-csd = true;

        spawn-at-startup = with pkgs-stable; [
          {command = ["${pkgs.firefox}/bin/firefox"];}
          {command = ["${swww}/bin/swww-daemon"];}
          {
            command = [
              "${webcord}/bin/webcord"
            ];
          }
          {
            command = [
              "${swww}/bin/swww"
              "img"
              "${config.periodic.wallpaper}"
            ];
          }
          {
            command = [
              "${wezterm}/bin/wezterm"
              "start"
              "--always-new-process"
            ];
          }
          {command = ["${pkgs.xwayland-satellite-unstable}/bin/xwayland-satellite"];}
        ];

        window-rules = [
          {
            clip-to-geometry = true;
            geometry-corner-radius = let
              radius = 15.0;
            in {
              bottom-left = radius;
              bottom-right = radius;
              top-left = radius;
              top-right = radius;
            };
          }
          {
            matches = [
              {app-id = "^org\.wezfurlong\.wezterm$";}
              {app-id = "^Alacritty$";}
            ];
            draw-border-with-background = false;
            open-on-workspace = "cmd";
          }
          {
            matches = [{app-id = "^firefox";}];
            open-fullscreen = true;
            open-on-workspace = "web";
          }
          {
            matches = [
              {app-id = "^discord";}
              {app-id = "^Element";}
              {app-id = "^org\.telegram\.desktop";}
              {app-id = "^WebCord";}
            ];
            open-on-workspace = "chat";
          }
          {
            matches = [{app-id = "^com\.github\.wwmm\.easyeffects";}];
            open-on-workspace = "av";
          }
          {
            matches = [{app-id = "^org\.qbittorrent\.qBittorrent";}];
            open-on-workspace = "dl";
          }
        ];

        workspaces = {
          "01-cmd" = {
            name = "cmd";
          };
          "02-web" = {
            name = "web";
          };
          "03-chat" = {
            name = "chat";
          };
          "04-game" = {
            name = "game";
          };
          "05-av" = {
            name = "av";
          };
          "06-dl" = {
            name = "dl";
          };
          "07-rand" = {
            name = "rand";
          };
        };
      };
    };
  };
}
