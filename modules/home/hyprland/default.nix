{
  lib,
  pkgs-stable,
  config,
  ...
}: {
  options.periodic = {
    hyprland.enable = lib.mkEnableOption "Enable hyprland.";
  };

  config = {
    periodic.hyprland.enable = lib.mkDefault false;
    programs = lib.mkIf config.periodic.hyprland.enable {
      fish.loginShellInit =
        # fish
        ''
          if test -z $WAYLAND_DISPLAY; and test (tty) = /dev/tty1
              Hyprland
          end
        '';
      zsh.loginExtra =
        lib.mkIf config.periodic.hyprland.enable # sh
        
        ''
          if [[ -z $WAYLAND_DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then Hyprland; fi
        '';
    };
    wayland.windowManager.hyprland = lib.mkIf config.periodic.hyprland.enable {
      enable = true;
      settings = {
        "$term" = "alacritty";
        "$mod" = "SUPER";
        "$kbd" = "model-f-labs-llc-f77-keyboard";
        bind = [
          # Go to workspace #n
          "$mod, 1, workspace, 1"
          "$mod, 2, workspace, 2"
          "$mod, 3, workspace, 3"
          "$mod, 4, workspace, 4"
          "$mod, 5, workspace, 5"
          "$mod, 6, workspace, 6"
          "$mod, 7, workspace, 7"
          "$mod, 8, workspace, 8"
          "$mod, 9, workspace, 9"
          "$mod, 0, workspace, 10"

          # Move to workspace #n
          "$mod SHIFT, 1, movetoworkspace, 1"
          "$mod SHIFT, 2, movetoworkspace, 2"
          "$mod SHIFT, 3, movetoworkspace, 3"
          "$mod SHIFT, 4, movetoworkspace, 4"
          "$mod SHIFT, 5, movetoworkspace, 5"
          "$mod SHIFT, 6, movetoworkspace, 6"
          "$mod SHIFT, 7, movetoworkspace, 7"
          "$mod SHIFT, 8, movetoworkspace, 8"
          "$mod SHIFT, 9, movetoworkspace, 9"
          "$mod SHIFT, 0, movetoworkspace, 10"

          # Window Cycling and Moving
          "$mod, J, cyclenext"
          "$mod, K, cyclenext, prev"
          "$mod SHIFT, J, swapnext"
          "$mod SHIFT, K, swapnext, prev"

          # Fullscreen
          "$mod, F, fullscreen"

          # Start Programs
          "$mod, R, exec, rofi -show drun -show-icons"
          "$mod, E, exec, emacs"
          "$mod, B, exec, firefox"
          # "$mod, T, exec, $term"
          "$mod, T, exec, [float;tile] wezterm start --always-new-process"

          # Kill Current Window
          "$mod, W, killactive"

          # Exit
          "$mod SHIFT, Q, exit"
        ];

        bindm = [
          # Moving windows by mouse
          "$mod, mouse:272, movewindow"
          "$mod, mouse:273, resizewindow"
        ];

        exec-once = [
          "${pkgs-stable.swww}/bin/swww-daemon && ${pkgs-stable.swww}/bin/swww img ${config.periodic.wallpaper} &"
          "${pkgs-stable.wlr-randr}/bin/wlr-randr setcursor ${config.periodic.cursor.theme} ${toString config.periodic.cursor.size} &"
        ];

        cursor = {
          inactive_timeout = 5;
        };

        input = {
          kb_layout = "us, se, pl";
          kb_variant = "dvorak, dvorak, dvorak";
          kb_options = "caps:ctrl_modifier";
        };

        dwindle = {
          no_gaps_when_only = 1;
        };

        misc = {
          disable_hyprland_logo = true;
          disable_splash_rendering = true;
          enable_swallow = true;
          swallow_regex = "^(Alacritty|org.wezfurlong.wezterm)$";
        };

        windowrule = [
          "workspace 1, ^(Alacritty)"
          "workspace 1, ^(org.wezfurlong.wezterm)"
          "workspace 1, ^(emacs)"
          "workspace 2, ^(Firefox)"
          "workspace 3, ^(TelegramDesktop)"
          "workspace 3, ^(Slack)"
          "workspace 3, ^(Element)"
          "workspace 3, ^(Discord)"
          "workspace 3, ^(teams-for-linux)"
        ];

        workspace = [
          "1, on-created-empty, default:true"
          "2, on-created-empty:firefox"
        ];
      };
    };
  };
}
