{lib, ...}: {
  options.periodic.wallpaper = lib.mkOption {
    description = "Sets the wallpaper file.";
    type = lib.types.path;
  };
}
