{...}: {
  programs.jujutsu = {
    enable = true;
    settings = {
      ui.default-command = "log";
      user = {
        email = "kacperuminski@protonmail.com";
        name = "Kacper Uminski";
      };
    };
  };
}
