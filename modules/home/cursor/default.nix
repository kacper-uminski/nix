{lib, ...}: {
  options.periodic.cursor = {
    size = lib.mkOption {
      description = "Sets the cursor size.";
      type = lib.types.int;
      default = 24;
    };
    theme = lib.mkOption {
      description = "Sets the cursor theme.";
      type = lib.types.str;
      default = "Breeze";
    };
  };
}
