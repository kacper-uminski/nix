{...}: {
  # SSH replacement for unreliable networks.
  programs.mosh.enable = true;
  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
  };
  users.users.kacper.openssh.authorizedKeys.keys = [
    # Only the username is necessary after the key.
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILYRDX4egfKkNnH4TThPOrLUdNeps8H3CfAuGJNBpZYK kacper"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIm6Hn3pV8uQDy3TjZRlViOPBqkrJBAY3+NmSxL1D/ou kacper"
  ];
}
