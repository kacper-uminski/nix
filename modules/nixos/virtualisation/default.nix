{
  lib,
  config,
  ...
}: {
  options.periodic = {
    virtualisation.enable = lib.mkEnableOption "Enable virtualisation.";
  };
  config = {
    periodic.virtualisation.enable = lib.mkDefault false;
    virtualisation = lib.mkIf config.periodic.virtualisation.enable {
      libvirtd.enable = true;
      spiceUSBRedirection.enable = true;
    };
  };
}
