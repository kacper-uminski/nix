{
  config,
  lib,
  pkgs,
  ...
}: {
  options.periodic = {
    qmk.enable = lib.mkEnableOption "Enable QMK firmware.";
  };

  config = {
    periodic.qmk.enable = lib.mkDefault false;

    environment = lib.mkIf config.periodic.qmk.enable {
      systemPackages = with pkgs; [
        dfu-programmer
        qmk
        vial
      ];
    };
    services.udev.packages = lib.mkIf config.periodic.qmk.enable [
      # pkgs.qmk-udev-rules
    ];
  };
}
