{pkgs, ...}: {
  users.users.kacper = {
    description = "Kacper Uminski";
    extraGroups = [
      "dialout"
      "wheel"
    ];
    isNormalUser = true;
    shell = pkgs.fish;
  };
  programs.zsh.enable = true;
  programs.fish.enable = true;
}
