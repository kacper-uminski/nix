{pkgs-stable, ...}: {
  programs.nh = {
    enable = true;
    package = pkgs-stable.nh;
    flake = "/home/kacper/.config/nix";
    clean = {
      enable = true;
      extraArgs = "--keep 10";
    };
  };
}
