{pkgs-stable, ...}: {
  documentation.dev.enable = true;
  environment.systemPackages = with pkgs-stable; [
    man-pages
    man-pages-posix
  ];
}
