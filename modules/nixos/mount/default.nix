{
  lib,
  config,
  ...
}: {
  options.periodic = {
    mount.nas.enable = lib.mkEnableOption "Enable mounting NAS.";
  };
  config = {
    periodic.mount.nas.enable = lib.mkDefault false;
    fileSystems = lib.mkIf config.periodic.mount.nas.enable {
      "/home/kacper/Media" = {
        device = "192.168.50.200:/mnt/Tank/Media";
        fsType = "nfs";
      };
    };
  };
}
