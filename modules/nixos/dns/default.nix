{
  config,
  lib,
  ...
}: {
  options.periodic = {
    dns-server.enable = lib.mkEnableOption "Enable DNS server for blocking ads.";
  };

  config = {
    periodic.dns-server.enable = lib.mkDefault false;

    # Web console is exposed on port 5380
    # Adding blocklists can be done in Settings/Blocking
    # Use quick-add with one of the recommended profiles.
    services.technitium-dns-server = lib.mkIf config.periodic.dns-server.enable {
      enable = true;
      openFirewall = true;
    };
  };
}
