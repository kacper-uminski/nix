{pkgs, ...}: {
  fonts.packages = with pkgs; [
    barlow
    # fira
    # fira-code
    line-awesome
    garamond-libre
    ibm-plex
    nerd-fonts.blex-mono
    uiua386
  ];
}
