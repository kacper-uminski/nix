{
  config,
  lib,
  ...
}: {
  options.periodic = {
    backlight.enable = lib.mkEnableOption "Enable backlight control.";
  };
  config = {
    periodic.backlight.enable = lib.mkDefault false;
    programs.light.enable = config.periodic.backlight.enable;
    users.users.kacper.extraGroups = lib.mkIf config.periodic.backlight.enable ["video"];
  };
}
