{...}: {
  environment = {
    pathsToLink = [
      "/share/bash-completion"
      "/share/zsh"
    ];

    sessionVariables = {
      NIXOS_OZONE_WL = "1";
    };
  };
}
