{...}: {
  boot.loader = {
    systemd-boot.enable = true;
    efi = {
      efiSysMountPoint = "/boot";
      canTouchEfiVariables = true;
    };
  };
}
