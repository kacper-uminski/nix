{
  config,
  lib,
  ...
}: {
  options.periodic = {
    networkmanager.enable = lib.mkEnableOption "Enable networkmanager.";
  };
  config = {
    periodic.networkmanager.enable = lib.mkDefault false;
    networking.networkmanager.enable = config.periodic.networkmanager.enable;
    users.users.kacper.extraGroups = lib.mkIf config.periodic.networkmanager.enable [
      "networkmanager"
    ];
  };
}
