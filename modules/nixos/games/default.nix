{
  config,
  lib,
  pkgs,
  ...
}: {
  options.periodic = {
    games.enable = lib.mkEnableOption "Enable games.";
  };
  config = {
    periodic.games.enable = lib.mkDefault false;

    programs.steam.enable = config.periodic.games.enable;

    environment = lib.mkIf config.periodic.games.enable {
      systemPackages = with pkgs; [
        retroarch
      ];
    };

    services.udev.packages = lib.mkIf config.periodic.games.enable [
      pkgs.game-devices-udev-rules
    ];
  };
}
