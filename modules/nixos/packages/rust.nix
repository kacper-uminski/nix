{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.rust.enable = lib.mkEnableOption "Enable Rust.";
  };
  config = lib.mkIf config.periodic.languages.rust.enable {
    environment.systemPackages = [
      pkgs-stable.rustup
      pkgs-stable.evcxr
    ];
  };
}
