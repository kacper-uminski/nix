{
  config,
  lib,
  pkgs,
  ...
}: {
  options.periodic = {
    languages.typst.enable = lib.mkEnableOption "Enable typst.";
  };
  config = lib.mkIf config.periodic.languages.typst.enable {
    environment.systemPackages = with pkgs; [
      typst
      typstyle
    ];
  };
}
