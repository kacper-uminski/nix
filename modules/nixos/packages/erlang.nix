{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.erlang.enable = lib.mkEnableOption "Enable Erlang.";
  };
  config = lib.mkIf config.periodic.languages.erlang.enable {
    environment.systemPackages = with pkgs-stable; [
      erlang
      erlang-ls
      rebar3 # Erlang build system.
    ];
  };
}
