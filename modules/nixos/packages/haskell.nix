{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.haskell.enable = lib.mkEnableOption "Enable Haskell.";
  };
  config = lib.mkIf config.periodic.languages.haskell.enable {
    environment.systemPackages = with pkgs-stable; [
      (haskellPackages.ghcWithPackages (
        hpkgs:
          with hpkgs; [
            haskell-language-server
          ]
      ))
    ];
  };
}
