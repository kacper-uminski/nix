{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.vhdl.enable = lib.mkEnableOption "Enable VHDL.";
  };
  config = lib.mkIf config.periodic.languages.vhdl.enable {
    environment.systemPackages = with pkgs-stable; [
      ghdl
      surfer
      psftools # Used for transforming font data.
      vhdl-ls
      xxd # Used to dump binary files in various ways.
    ];
  };
}
