{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.elixir.enable = lib.mkEnableOption "Enable Elixir.";
  };
  config = lib.mkIf config.periodic.languages.elixir.enable {
    environment.systemPackages = with pkgs-stable; [
      elixir
      elixir-ls
    ];
  };
}
