{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.python.enable = lib.mkEnableOption "Enable Python.";
  };
  config = lib.mkIf config.periodic.languages.python.enable {
    environment.systemPackages = with pkgs-stable; [
      (python3.withPackages (
        py-pkgs:
          with py-pkgs; [
            python-lsp-ruff
            python-lsp-server
            ruff-lsp
            tkinter
            websockets
          ]
      ))
      ruff
    ];
  };
}
