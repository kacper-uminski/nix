{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.c.enable = lib.mkEnableOption "Enable C/C++.";
  };
  config = lib.mkIf config.periodic.languages.c.enable {
    environment.systemPackages = with pkgs-stable; [
      bear # Allows LSP to find #include non-std files and headers.
      llvmPackages_19.clang-tools
      gcc14
      gdb
      gnumake
      valgrind # Memory profiler for C/C++
    ];
  };
}
