{
  config,
  lib,
  pkgs-stable,
  ...
}: {
  options.periodic = {
    languages.java.enable = lib.mkEnableOption "Enable Java.";
  };
  config = lib.mkIf config.periodic.languages.java.enable {
    programs.java.enable = true;
    environment.systemPackages = with pkgs-stable; [
      jetbrains.idea-community
      maven
    ];
  };
}
