{
  pkgs,
  pkgs-stable,
  ...
}: {
  imports = [
    ./c.nix
    ./elixir.nix
    ./erlang.nix
    ./haskell.nix
    ./java.nix
    ./nix.nix
    ./python.nix
    ./rust.nix
    ./typst.nix
    ./vhdl.nix
  ];
  config = {
    periodic.languages = {
      c.enable = true;
      elixir.enable = false;
      erlang.enable = false;
      haskell.enable = true;
      java.enable = true;
      python.enable = true;
      rust.enable = true;
      typst.enable = true;
      vhdl.enable = false;
    };
    environment.systemPackages = with pkgs-stable; let
      resmv = writers.writeDashBin "resmv" ''mv $1 $(${file}/bin/file $1 | ${ripgrep}/bin/rg -o [0-9]+x[0-9]+ | tail -1).jpg'';
    in [
      bat
      cyme # lsusb alternative
      devenv
      element-desktop
      eww
      exercism
      fd
      ffmpeg
      file
      pkgs.firefox
      fzf
      gimp
      halloy
      home-manager
      inkscape
      pandoc
      pdf2svg
      resmv
      pkgs.sacd_extract
      screen
      slack
      speedtest-rs
      sshfs
      swww
      tdesktop
      teams-for-linux
      tokei
      uiua
      unzip
      vifm
      webcord
      wget
      wl-clipboard-rs
      wlr-randr
      zip
    ];
  };
}
