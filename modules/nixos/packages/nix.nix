{pkgs-stable, ...}: {
  environment.systemPackages = with pkgs-stable; [
    alejandra
    nil
    nixfmt-rfc-style
  ];
}
