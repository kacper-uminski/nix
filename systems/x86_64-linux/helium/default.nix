{
  inputs,
  pkgs,
  ...
}: {
  imports = [
    inputs.nixos-hardware.nixosModules.framework-13-7040-amd
    ./hardware-configuration.nix
  ];

  periodic = {
    backlight.enable = true;
    networkmanager.enable = true;
  };

  environment.systemPackages = with pkgs; [
    acpi
    light
    networkmanagerapplet
    wireplumber
  ];

  networking = {
    hostName = "helium";
    nameservers = ["1.1.1.1"];
  };

  services.fwupd.enable = true;

  system.stateVersion = "24.05";
}
