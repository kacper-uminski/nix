{pkgs, ...}: {
  imports = [
    ./hardware-configuration.nix
  ];

  periodic = {
    dns-server.enable = true;
    games.enable = true;
    mount.nas.enable = true;
    qmk.enable = true;
  };

  boot = {
    initrd.kernelModules = ["amdgpu"];
    kernelModules = ["msr"];
  };

  environment.systemPackages = with pkgs; [
    cuetools
    darktable
    easyeffects
    easytag
    flac
    mcomix
    msr # Used by xmrig.
    prismlauncher
    puddletag
    pulsemixer
    qbittorrent
    qmk
    screen
    shntool
    xmrig
  ];

  networking = {
    hostName = "hydrogen";
    firewall.enable = false;
    defaultGateway = "192.168.50.1";
    nameservers = ["192.168.50.250"];
    interfaces.enp12s0.ipv4.addresses = [
      {
        address = "192.168.50.250";
        prefixLength = 24;
      }
    ];
  };

  services.smartd.enable = true;

  system.stateVersion = "24.11";
}
