{
  config,
  inputs,
  lib,
  pkgs,
  pkgs-stable,
  ...
}: let
  # Uncomment when pipes are supported by alejandra.
  # default-importer = path: builtins.readDir path |> builtins.attrNames |> builtins.map (p: path + "/${p}/default.nix");
  default-importer = path: builtins.map (p: path + "/${p}/default.nix") (builtins.attrNames (builtins.readDir path));
in {
  options = {
    home-file = lib.mkOption {
      type = lib.types.str;
      description = "home-manager file to import";
    };
    # themes = {
    #   base16.enable = lib.mkDefault false;
    #   challenger-deep.enable = lib.mkDefault false;
    # };
  };

  imports =
    (default-importer ./modules/nixos)
    ++ [
      inputs.home-manager.nixosModules.home-manager
      {
        home-manager = {
          backupFileExtension = "backup";
          useGlobalPkgs = true;
          useUserPackages = true;
          users.kacper = import ./homes/x86_64-linux/${config.home-file}/default.nix;
          extraSpecialArgs = {
            inherit
              default-importer
              inputs
              pkgs-stable
              ;
          };
        };
      }
      inputs.niri.nixosModules.niri
    ];

  config = {
    nix.settings = {
      experimental-features = [
        "flakes"
        "nix-command"
        "pipe-operators"
      ];
      use-xdg-base-directories = true;
    };
    nixpkgs = {
      config.allowUnfree = true;
      overlays = [
        inputs.niri.overlays.niri
        (prev: next: {
          sacd_extract = pkgs.callPackage ./packages/sacd_extract/default.nix {};
        })
      ];
    };
  };
}
